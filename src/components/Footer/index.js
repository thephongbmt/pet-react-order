import React, { memo } from 'react';
const Footer = ({ description='This is Footer', ...props }) => {
  return (
  <div>{description}</div>
  )
}

export default memo(Footer);