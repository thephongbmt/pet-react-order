import MenuDashBoard from '../MenuDashBoard';
import React ,{memo} from 'react';

const hanldeRenderMenus = (menus) => {
  return menus.map(({to,title,...props} )=> <MenuDashBoard key={to} to={to} title={title} {...props} />)
}

const MenuDashBoardList = ({ menus= [], ...props}) => {
  return <div>
    { hanldeRenderMenus(menus) }
  </div>
}

export default memo(MenuDashBoardList);