import React, { memo } from 'react';

const Button = memo(({ title='Submit', ...rest}) => {
  return (
    <button {...rest}>{title}</button>
  )
})

export default Button;