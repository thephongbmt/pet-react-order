import React, { memo } from 'react';
const TextBox = memo(({placeholder = 'Typing something',...rest}) => {
  console.log(placeholder)
  return (
    <input type="textbox" placeholder={placeholder} {...rest} />
  )
})

export default TextBox;