import NavigateRoute from './NavigateRoute';
import { getLocalStorage, KEY } from '../../helper/localStorage';
let PrivateRoute = NavigateRoute(() => getLocalStorage(KEY.ACCESS_TOKEN),'/login')
export default PrivateRoute;
