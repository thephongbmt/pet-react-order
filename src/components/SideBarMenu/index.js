import React, { memo } from 'react';
import MenuDashBoardList from '../MenuDashBoardList';

const SideBarMenu = (props) => {
  return (
    <div>
      <MenuDashBoardList {...props} />
    </div>
  )
}

export default memo(SideBarMenu)