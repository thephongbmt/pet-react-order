import React, { memo } from 'react';
import { Link } from 'react-router-dom';
const Menu = ({ title = 'Menu A', to = '/' , ...props }) => {
  return (
  <div>
    <Link to = {to}>{title}</Link>
  </div>
  )
}
export default memo(Menu);