import React, { memo } from 'react';
import {
  Switch,
  BrowserRouter as Router,
} from 'react-router-dom'
import PublicRoute from '../components/NavigateRoute/PulbicRoute';
import PrivateRoute from '../components/NavigateRoute/PrivateRoute';
import AdminLayout from '../components/AdminLayout';
// import localStorageHelper from '../helper/localStorage';
import * as LoadAbleRoute from './loadable';
const initRoute = [
  { path: '/login', isAuthenRoute:false, component: LoadAbleRoute.LoginLoadable, isExact: true},
  { path: '/', isAuthenRoute: true , component: LoadAbleRoute.DashBoardLoadable, isExact: true},
  { path: '/order', isAuthenRoute:true, component: LoadAbleRoute.OrderLoadable, isExact: true},
]

const filterRouteType = (routes) => {
  return {
    publicRoutes: routes.filter(route => route.isAuthenRoute === false),
    privateRoutes: routes.filter(route => route.isAuthenRoute === true),
  }
}
const BuildRoute = ({isAuthenRoute,routes}) => {
  console.log("routes=>",routes)
  if(isAuthenRoute){
    let routesRender = routes.map((route) => <PrivateRoute key={route.path} exact={route.isExact} component={route.component} />);
    return <AdminLayout>
      {routesRender}
    </AdminLayout>
  }
  return routes.map((route) => {
    return <PublicRoute  key={route.path} exact={route.isExact} component={route.component}/>
  })
}
const InitPrivateRoutes =  (props) => {
  console.log("InitPrivateRoutes --->",props.routes);
  return <Switch><BuildRoute isAuthenRoute={true}  {...props} /></Switch>
}

const InitPublicRoutes = (props) => {
  console.log("InitPublicRoutes --->",props.routes);
  return <Switch> <BuildRoute isAuthenRoute={false}  {...props} /></Switch>
}

const InitRoute = () => {
  let { publicRoutes, privateRoutes } = filterRouteType(initRoute);
  return (
    <React.Suspense fallback={"..."}>
      <Router>

        <InitPrivateRoutes routes = {privateRoutes} />
        <InitPublicRoutes routes = {publicRoutes} />
    
      </Router>
    </React.Suspense>
  )
}

export default InitRoute;