import {lazy} from 'react';
export const DashBoardLoadable = lazy(()=> import('../container/DashBoard'));
export const LoginLoadable = lazy(()=> import('../container/Login'));
export const OrderLoadable = lazy(()=> import('../container/Order'));