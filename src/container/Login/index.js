
import React from 'react';
import { useHistory } from "react-router-dom";
import TextBox from '../../components/TextBox';
import Button from '../../components/Button';
import {setLocalStorage,KEY} from "../../helper/localStorage.js"
const Login = function () {
  let history = useHistory();
  const handleLogin = () => {
    setLocalStorage(KEY.ACCESS_TOKEN, "asasas");
    history.push("/");

  }
  return (
    <div className='wrapper-login'>
      <div className='cotainer-login-form'>
        <div className='header-login-form'>
          <h1>Login Form</h1>
        </div>  
        <div className='login-form'>
          <TextBox placeholder='Email' />
          <TextBox placeholder='Password' />
          <Button title={'Login'} onClick={handleLogin}/> 
        </div>
      </div>
    </div>
  )
}

export default Login;